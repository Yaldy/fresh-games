let users = []
let newusers = []

// consultar por todos los usuarios
exports.getUsers = () => {
    return users
}

// Consultar por un usuario 
exports.getOneUser = (nameUser) => {
    return users.find((user) => {
        return user.name === nameUser
    })
}
/*
exports.getOneUser = (nameUser) => {
    let user = users.find((user) => {
        return user.name === nameUser
    })
    return user.mail
}
*/
exports.getOneUserWithParams = (nameUser,mail) => {
    return users.find((user) => {
        return (user.name === nameUser && user.mail === mail)
    })
}

//Actualizar info de un usuario
exports.updateUser = (newDataUser, oldNameUser, oldPassUser) => {
    let aprov = 0
    users.find((user) => {
        if(user.name === oldNameUser && user.password === oldPassUser){
            if(newDataUser.name != undefined){
                user.name = newDataUser.name
            }
            if(newDataUser.password != undefined){
                user.password = newDataUser.password
            }
            if(newDataUser.mail != undefined){
                user.mail = newDataUser.mail
            }
            if(newDataUser.location != undefined){
                user.location = newDataUser.location
            }
            aprov = 1
        } 
    })
    return aprov
}


//Agregar usuario
exports.addUser = (userData) => {
    const user = {
        name: userData.name,
        mail: userData.mail,
        password: userData.password,
        location: userData.location
        //foto: userData.foto
    }
    users.push(user)
}

//Eliminar un usuario
exports.removeUser = (userMail) => {
    users = users.filter((user) => {
        return user.mail !== userMail
    })
}
