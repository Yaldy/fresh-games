import userActions from '../../actions/user/user'
//READ
exports.getAllUsers = (ctx) => {
    ctx.body = userActions.getUsers()
    return ctx
}

exports.getUser = (ctx) => {
    ctx.body = userActions.getOneUser(ctx.params.nameUser)
    return ctx
}

exports.getMail = (ctx) => {
    let user = userActions.getOneUser(ctx.params.nameUser)
    ctx.body = {
        "mail": user.mail
    }
    return ctx
}

exports.getUserWithTwoParams = (ctx) => {
    ctx.body = userActions.getOneUserWithParams(ctx.params.nameUser,ctx.params.mail)
    return ctx
}



//CREATE
exports.createUser = (ctx) => {
    if(!ctx.request.body.name || !ctx.request.body.password || !ctx.request.body.mail || !ctx.request.body.location){
        ctx.status = 400
        ctx.body = {message: 'Debe incluir todos los campos'}
        return ctx
    }
    userActions.addUser(ctx.request.body)
    ctx.body = { message: 'User was created' }
    return ctx
}

//UPDATE

exports.updateUser = (ctx) => {
    if(!ctx.request.body.name || !ctx.request.body.password || !ctx.params.nameUser || !ctx.params.passUser){
        ctx.status = 400
        ctx.body = {message: 'Debe incluir todos los campos'}
    }
    ctx.body = userActions.updateUser(ctx.request.body, ctx.params.nameUser, ctx.params.passUser)
    if(ctx.body == 1){
        ctx.status = 200
        ctx.body = {message: 'Información actualizada'}
    }
    else{
        ctx.status = 400
        ctx.body = {message: 'Error al actualizar la información, confirme que el nombre de usuario y contraseña fueron ingresados correctamente'}
    }
    return ctx
}

//DELETE
exports.removeUser = (ctx) => {
    userActions.removeUser(ctx.params.userMail)
    ctx.body = { message: 'User was removed' }
    return ctx
}
