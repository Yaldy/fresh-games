import commentsActions from '../../actions/comments/comments'

//READ
exports.getComment = (ctx) => {
    ctx.body = commentsActions.getComments(ctx.params.id_user1)
    return ctx
}
//CREATE
exports.createComment = (ctx) => {
    //se debería verificar si existen los usuarios
    commentsActions.addComment(ctx.request.body)
    ctx.body = { message: 'Comment was created' }
    return ctx
}

//DELETE
exports.removeComment = (ctx) => {
    let flag = commentsActions.removeComment(ctx.params.id_comentario)
    if(flag==0){
        ctx.status = 400
        ctx.body = {message: "No se encontró id de comentario"}
    }
    else {
        ctx.status = 200
        ctx.body = { message: 'Comment was removed' }
    }
    return ctx.body
}
