import publicacionActions from '../../actions/publicaciones/publicaciones'

// READ
exports.getAllPublicaciones = (ctx) => {
    ctx.body = publicacionActions.getPublicaion()
    return ctx
}

// CREATE
exports.createPublicacion = (ctx) => {
    publicacionActions.addPublicacion(ctx.request.body)
    ctx.body = { message: 'Publicacion a sido creada' }
    return ctx
}

// DELETE
exports.removePublicacion = (ctx) => {
    publicacionActions.removePublicacion(ctx.params.publicacionName)
    ctx.body = { message: 'Publicacion a sido removida' }
    return ctx
}

// UPDATE
exports.updatePublicacion = (ctx) => {
    const { publicacionName } = ctx.params;
    const newData = ctx.request.body;
    const updated = publicacionActions.updatePublicacion(publicacionName, newData);
    if (updated) {
        ctx.body = { message: 'Publicacion a sido actualizada' }
    } else {
        ctx.status = 404;
        ctx.body = { error: 'Publicacion no encontrada' }
    }
    return ctx;
}

// READ ONE
exports.getPublicacionByName = (ctx) => {
    const { publicacionName } = ctx.params;
    const publicacion = publicacionActions.getPublicacionByName(publicacionName);
    if (publicacion) {
        ctx.body = publicacion;
    } else {
        ctx.status = 404;
        ctx.body = { error: 'Publicacion no encontrada' }
    }
    return ctx;
}
