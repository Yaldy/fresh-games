let comments = []

//Agregar cometario (c)
exports.addComment = (comentario) => {
    const newComment = {
        id: comments.length == 0? 1:comments[comments.length-1].id+1,// asignar id de manera incremental e irrepetible
        id_user1: comentario.id_user1, //al que le comentan
        id_user2: comentario.id_user2, //el que comenta
        texto: comentario.texto
        //foto: comentario.foto
        //valoración: comentario.valoracion
    }
    comments.push(newComment)
}

//extraer comentarios de usuario x (r)
exports.getComments = (user1) => {
    let comentarios_encontrados = comments.filter((comentario) => {
        return comentario.id_user1 == user1
    })
    return comentarios_encontrados
    //console.log(user)

}

// (u) por ahora no se da la posibilidad de editar/actualizar comentario

//Eliminar comentario (d)
exports.removeComment = (id_comentario) => {
    let len= comments.length
    comments = comments.filter((comentario) => {
        return (comentario.id != id_comentario)
    })
    if(len==comments.length){ //fracaso
        return 0
    }
    else{ //exito
        return 1
    }
}
