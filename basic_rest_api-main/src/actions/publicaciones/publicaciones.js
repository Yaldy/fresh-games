let publicaciones = []
let newpublicaion = []


// consultar por todos las publicaciones
exports.getPublicaion = () => {
    return publicaciones
}

// Consultar una sola publicación por su nombre
exports.getPublicacionByName = (PublicacionName) => {
    const publicacion = publicaciones.find((pub) => pub.name === PublicacionName);
    return publicacion; 
}


//extraer info de publicaciones

//Agregar publicacion
exports.addPublicacion = (PublicacionData) => {
    const publicacion = {
        name: PublicacionData.name,
        DueñoDelJuego : PublicacionData.dueño,
        valoracion : PublicacionData.valoracion,
        ventaOcambio : PublicacionData.ventaOcambio,
        location: PublicacionData.location,
        estado: PublicacionData.estado,
        info: PublicacionData.info,
        foto: PublicacionData.foto
    }
    publicaciones.push(publicacion)
}

// Modificar datos de una publicación existente
exports.updatePublicacion = (PublicacionName, newData) => {
    const index = publicaciones.findIndex((publicacion) => {
        return publicacion.name === PublicacionName
    })

    if (index !== -1) {
        publicaciones[index] = {
            ...publicaciones[index], 
            ...newData 
        }
        return true; 
    } else {
        return false; 
    }
}


exports.removePublicacion = (PublicacionName) => {
    publicacion = publicaciones.filter((publicacion) => {
        return publicacion.name !== PublicacionName
    })
}
